﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Redledge.KuehneNagel.IoC.MicrosoftExtensionsDependencyInjection
{
    public static class Container
    {
        public static IConfiguration GetConfiguration(string basePath)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json");

            return builder.Build();
        }

        public static ServiceProvider CreateServiceCollection(IConfiguration configuration)
        {
            var services = new ServiceCollection();

            return CreateServiceCollection(configuration, services);
        }

        public static ServiceProvider CreateServiceCollection(IConfiguration configuration, IServiceCollection services)
        {
            services.WireUpApplication(configuration);

            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider;
        }

    }
}
