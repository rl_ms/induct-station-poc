﻿using System;
using System.Collections.Generic;

namespace Redledge.KuehneNagel.Windows.EventArguments
{
    public class TagReadEventArgs : EventArgs
    {
        public List<string> Tags { get; set; }
    }
}
