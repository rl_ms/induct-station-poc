﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;

using Redledge.KuehneNagel.Windows.EventArguments;
using Redledge.KuehneNagel.Windows.Interfaces.Managers;
using Redledge.KuehneNagel.Windows.POCOs;

namespace Redledge.KuehneNagel.Windows
{
    public partial class InductStationForm : Form
    {
        #region Private Properties

        private readonly IRFIDReaderManager _reader;

        private bool _allowScan;
        private List<TagReadDTO> _tags;
        private bool _started;
        private TimeSpan _scanDuration;
        private DateTime? _startTime;
        private DateTime? _lastTagReadTime;
        private static bool _formDisposed;

        #endregion

        #region ctors

        public InductStationForm(IRFIDReaderManager readerManager)
        {
            InitializeComponent();

            _tags = new List<TagReadDTO>();

            _reader = readerManager;
            _reader.TagRead += Reader_TagRead;

            TagReadTimer.Interval = 50;
            TagReadTimer.Tick += TagReadTimer_Tick;
        }

        #endregion

        #region Public Properties

        public int? AutoResetAfter => AutoResetAfterToggleSwitch.IsOn ? Convert.ToInt32(AutoResetAfterNumericUpDown.Value) : (int?)null;

        public int? ExpectedTags => ExpectedTagsToggleSwitch.IsOn ? Convert.ToInt32(ExpectedTagsNumericUpDown.Value) : (int?)null;

        public int ReadCount
        {
            get => int.Parse(ReadCountLabel.Text);
            set => ReadCountLabel.Text = value.ToString();
        }

        public TimeSpan ScanTime
        {
            set => ScanTimerValueLabel.Text = value.ToString(@"hh\:mm\:ss\.fff");
        }

        public string Tags
        {
            get => TagsTextBox.Text;
            set => TagsTextBox.Text = value;
        }

        #endregion

        #region Private Methods

        private void FormatControls()
        {
            var optimusColour = Color.FromArgb(69, 150, 213);

            HeaderTableLayoutPanel.BackColor = optimusColour;
            OptimusSortersPanel.BackColor = optimusColour;

            // DevExpress
            var element = SkinManager.GetSkinElement(SkinProductId.Editors, UserLookAndFeel.Default, "ToggleSwitch");
            var image = new Bitmap(".\\toggleswitch.png");
            element.Image.SetImage(image, Color.Transparent);
            LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
        }

        private void HandleError(Exception exception)
        {
            MessageBox.Show(exception.Message, $"Error: {exception.Source}", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private delegate void RefreshFormDelegate();

        private void RefreshForm()
        {
            if (this.InvokeRequired && !_formDisposed)
            {
                this.Invoke(new RefreshFormDelegate(RefreshForm));
                return;
            }

            // Visuals
            BackColor = (ExpectedTags.HasValue && _tags.Count >= ExpectedTags.Value)
                ? Color.Chartreuse
                : _started ? Color.Gold : Color.White;

            AutoResetAfterNumericUpDown.Enabled = AutoResetAfter.HasValue;
            ExpectedTagsNumericUpDown.Enabled = ExpectedTags.HasValue;
            StartStopButton.BackColor = _started ? Color.Red : Color.Chartreuse;
            StartStopButton.ForeColor = _started ? Color.White : SystemColors.ControlText;

            // Values
            ReadCount = _tags.Count;
            if (_allowScan) ScanTime = _scanDuration;
            StartStopButton.Text = _started ? "Stop" : "Start";

            var tagList = _tags
                .OrderByDescending(t => t.ReadTime)
                .Select(t => $"{t.Value} @ {t.ReadTime.ToShortDateString()} {t.ReadTime.ToString("hh:mm:ss.ms")} (elapsed: {(t.ReadTime - _startTime.Value).ToString(@"mm\:ss\.fff")})");
            Tags = string.Join("\r\n", tagList);
        }

        private void Reset()
        {
            if (_started)
                Stop();

            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            _allowScan = true;
            _lastTagReadTime = null;
            _scanDuration = TimeSpan.Zero;
            _startTime = null;
            _tags.Clear();
        }

        private void Start()
        {
            _allowScan = true;
            _lastTagReadTime = DateTime.Now;
            _scanDuration = TimeSpan.Zero;
            _startTime = _lastTagReadTime;
            _tags.Clear();

            if (!_reader.Connect()) throw new Exception("Cannot connect to the RFID reader.\n\nPlease check that the RFID reader is connected.");

            _started = true;

            TagReadTimer.Start();
        }

        private void Stop()
        {
            _reader.Disconnect();
            TagReadTimer.Stop();

            _started = false;
        }

        #endregion

        #region Events

        private void Control_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshForm();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void InductStationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // BAIL if we are not scanning
                if (!_started) return;

                // ASK user, stop scanning?
                var response = MessageBox.Show("Scan in progress. Stop scanning?", "Stop scanning?", MessageBoxButtons.YesNo);

                // BAIL if No
                if (response != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }

                Stop();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void InductStationForm_Load(object sender, EventArgs e)
        {
            try
            {
                FormatControls();

                RefreshForm();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void Reader_TagRead(object sender, TagReadEventArgs e)
        {
            try
            {
                if (!_allowScan) return;


                foreach (var tag in e.Tags)
                    if (!_tags.Any(t => t.Value == tag))
                    {
                        _lastTagReadTime = DateTime.Now;
                        _tags.Add(new TagReadDTO { ReadTime = _lastTagReadTime.Value, Value = tag });
                    }

                RefreshForm();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            try
            {
                Reset();

                RefreshForm();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void StartStopButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (_started)
                    Stop();
                else
                    Start();
 
                RefreshForm();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void TagReadTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                _scanDuration = DateTime.Now - _startTime.Value;
                var idleTime = DateTime.Now - _lastTagReadTime.Value;

                if (AutoResetAfter.HasValue && idleTime.TotalSeconds >= AutoResetAfter.Value)
                {
                    Reset();
                    Start();
                }

                if (ExpectedTags.HasValue && _tags.Count >= ExpectedTags.Value)
                {
                    if (AutoResetAfter.HasValue)
                        _allowScan = false;
                    else
                        Stop();
                }

                RefreshForm();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        #endregion

    }
}
