﻿using System;
using System.Collections.Generic;
using System.Timers;

using Redledge.KuehneNagel.Windows.EventArguments;
using Redledge.KuehneNagel.Windows.Interfaces.Managers;

namespace Redledge.KuehneNagel.Windows.Managers
{
    public class FakeRFIDReaderManager : IRFIDReaderManager
    {
        #region Private Properties

        private Timer _timer;

        #endregion

        #region ctors

        public FakeRFIDReaderManager()
        {
            _timer = new Timer();
            _timer.Elapsed += _timer_Elapsed;
            _timer.Interval = 1000;
        }

        #endregion

        #region Private methods

        #endregion

        #region Public Methods

        public bool Connect()
        {
            _timer.Start();

            return true;
        }

        public bool Disconnect()
        {
            _timer.Stop();

            return true;
        }

        public bool IsConnected() => true;

        #endregion

        #region Events

        public event EventHandler<TagReadEventArgs> TagRead;

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var tags = new List<string> { Guid.NewGuid().ToString() };
            var tagReadEventArgs = new TagReadEventArgs { Tags = tags };

            TagRead?.Invoke(this, tagReadEventArgs);
        }

        #endregion
    }
}
