﻿using System;
using System.Collections.Generic;
using System.Linq;
using NurApiDotNet;

using Redledge.KuehneNagel.Windows.EventArguments;
using Redledge.KuehneNagel.Windows.Interfaces.Managers;

namespace Redledge.KuehneNagel.Windows.Managers
{
    public class NordicRFIDReaderManager : IRFIDReaderManager
    {
        #region Private Properties

        private NurApi _nurApi;

        private bool _running;

        #endregion

        #region ctors

        public NordicRFIDReaderManager()
        {
            _nurApi = new NurApi();

            _nurApi.InventoryStreamEvent += NurApi_InventoryStreamEvent;

            _nurApi.SetUsbAutoConnect(true);
        }

        #endregion

        #region Public Methods

        public bool Connect()
        {
            try
            {
                _nurApi.ClearTags();

                // BAIL if not connected
                if (!_nurApi.IsConnected()) return false;

                _nurApi.TxLevel = 10;

                _nurApi.StartInventoryStream();

                _running = true;

                return true;
            }
            catch (Exception ex)
            {
                _running = false;
                return false;
            }
        }

        public bool Disconnect()
        {
            try
            {
                _nurApi.StopInventoryStream();
                _running = false;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsConnected()
        {
            try
            {
                return _nurApi?.IsConnected() ?? false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Events

        private void NurApi_InventoryStreamEvent(object sender, NurApi.InventoryStreamEventArgs e)
        {
            try
            {
                var tags = new List<string>();

                foreach (NurApi.Tag tag in _nurApi.GetTagStorage())
                    tags.Add(tag.GetEpcString());

                if (tags.Any())
                {
                    var tagReadEventArgs = new TagReadEventArgs { Tags = tags };

                    TagRead?.Invoke(this, tagReadEventArgs);
                }

                _nurApi.ClearTags();

                if (e.data.stopped && _running)
                {
                    _nurApi.StartInventoryStream();
                }
            }
            catch (Exception)
            {
                // SWALLOW
            }
        }

        private object List<T>()
        {
            throw new NotImplementedException();
        }

        public event EventHandler<TagReadEventArgs> TagRead;

        #endregion
    }
}
