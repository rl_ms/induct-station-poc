﻿using System;

namespace Redledge.KuehneNagel.Windows.POCOs
{
    public class TagReadDTO
    {
        public DateTime ReadTime { get; set; }
        public string Value { get; set; }
    }
}
