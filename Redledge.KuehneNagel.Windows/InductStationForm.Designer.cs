﻿namespace Redledge.KuehneNagel.Windows
{
    partial class InductStationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InductStationForm));
            this.TagReadTimer = new System.Windows.Forms.Timer(this.components);
            this.ControlsPanel = new System.Windows.Forms.Panel();
            this.ControlsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CustomerLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.StartStopButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.HeaderPanel = new System.Windows.Forms.Panel();
            this.HeaderTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.StartPictureBox = new System.Windows.Forms.PictureBox();
            this.CancelPictureBox = new System.Windows.Forms.PictureBox();
            this.AutoResetAfterPanel = new System.Windows.Forms.Panel();
            this.AutoResetAfterTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.AutoResetAfterNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.AutoResetAfterToggleSwitch = new DevExpress.XtraEditors.ToggleSwitch();
            this.AutoResetAfterTitleLabel = new System.Windows.Forms.Label();
            this.ExpectedTagsPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ExpectedTagsLabel = new System.Windows.Forms.Label();
            this.ExpectedTagsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ExpectedTagsToggleSwitch = new DevExpress.XtraEditors.ToggleSwitch();
            this.ScanDataTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ScanTimerValueLabel = new System.Windows.Forms.Label();
            this.ReadCountLabel = new System.Windows.Forms.Label();
            this.TagsTextBox = new System.Windows.Forms.TextBox();
            this.OurLogoPanel = new System.Windows.Forms.Panel();
            this.OurLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.OptimusSortersPanel = new System.Windows.Forms.Panel();
            this.OptimusSortersPictureBox = new System.Windows.Forms.PictureBox();
            this.ControlsPanel.SuspendLayout();
            this.ControlsTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerLogoPictureBox)).BeginInit();
            this.HeaderPanel.SuspendLayout();
            this.HeaderTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelPictureBox)).BeginInit();
            this.AutoResetAfterPanel.SuspendLayout();
            this.AutoResetAfterTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutoResetAfterNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoResetAfterToggleSwitch.Properties)).BeginInit();
            this.ExpectedTagsPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedTagsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedTagsToggleSwitch.Properties)).BeginInit();
            this.ScanDataTableLayoutPanel.SuspendLayout();
            this.OurLogoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OurLogoPictureBox)).BeginInit();
            this.OptimusSortersPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptimusSortersPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ControlsPanel
            // 
            this.ControlsPanel.BackColor = System.Drawing.Color.White;
            this.ControlsPanel.Controls.Add(this.ControlsTableLayoutPanel);
            this.ControlsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.ControlsPanel.Location = new System.Drawing.Point(964, 0);
            this.ControlsPanel.Name = "ControlsPanel";
            this.ControlsPanel.Size = new System.Drawing.Size(300, 985);
            this.ControlsPanel.TabIndex = 14;
            // 
            // ControlsTableLayoutPanel
            // 
            this.ControlsTableLayoutPanel.ColumnCount = 2;
            this.ControlsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ControlsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.ControlsTableLayoutPanel.Controls.Add(this.CancelPictureBox, 0, 6);
            this.ControlsTableLayoutPanel.Controls.Add(this.StartPictureBox, 0, 5);
            this.ControlsTableLayoutPanel.Controls.Add(this.CustomerLogoPictureBox, 0, 1);
            this.ControlsTableLayoutPanel.Controls.Add(this.StartStopButton, 1, 5);
            this.ControlsTableLayoutPanel.Controls.Add(this.ResetButton, 1, 6);
            this.ControlsTableLayoutPanel.Controls.Add(this.AutoResetAfterPanel, 0, 3);
            this.ControlsTableLayoutPanel.Controls.Add(this.ExpectedTagsPanel, 0, 4);
            this.ControlsTableLayoutPanel.Controls.Add(this.OurLogoPanel, 0, 2);
            this.ControlsTableLayoutPanel.Controls.Add(this.OptimusSortersPanel, 0, 0);
            this.ControlsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ControlsTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlsTableLayoutPanel.Name = "ControlsTableLayoutPanel";
            this.ControlsTableLayoutPanel.RowCount = 8;
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.ControlsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ControlsTableLayoutPanel.Size = new System.Drawing.Size(300, 985);
            this.ControlsTableLayoutPanel.TabIndex = 0;
            // 
            // CustomerLogoPictureBox
            // 
            this.CustomerLogoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlsTableLayoutPanel.SetColumnSpan(this.CustomerLogoPictureBox, 2);
            this.CustomerLogoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustomerLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("CustomerLogoPictureBox.Image")));
            this.CustomerLogoPictureBox.Location = new System.Drawing.Point(3, 153);
            this.CustomerLogoPictureBox.Name = "CustomerLogoPictureBox";
            this.CustomerLogoPictureBox.Size = new System.Drawing.Size(294, 194);
            this.CustomerLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CustomerLogoPictureBox.TabIndex = 0;
            this.CustomerLogoPictureBox.TabStop = false;
            // 
            // StartStopButton
            // 
            this.StartStopButton.BackColor = System.Drawing.Color.Chartreuse;
            this.StartStopButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StartStopButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartStopButton.Font = new System.Drawing.Font("Calibri", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartStopButton.Location = new System.Drawing.Point(108, 603);
            this.StartStopButton.Name = "StartStopButton";
            this.StartStopButton.Size = new System.Drawing.Size(189, 94);
            this.StartStopButton.TabIndex = 2;
            this.StartStopButton.Text = "Start";
            this.StartStopButton.UseVisualStyleBackColor = false;
            this.StartStopButton.Click += new System.EventHandler(this.StartStopButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.BackColor = System.Drawing.Color.Red;
            this.ResetButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResetButton.Font = new System.Drawing.Font("Calibri", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.ForeColor = System.Drawing.Color.White;
            this.ResetButton.Location = new System.Drawing.Point(108, 703);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(189, 94);
            this.ResetButton.TabIndex = 3;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = false;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.BackColor = System.Drawing.Color.Black;
            this.HeaderPanel.Controls.Add(this.HeaderTableLayoutPanel);
            this.HeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Size = new System.Drawing.Size(964, 48);
            this.HeaderPanel.TabIndex = 15;
            // 
            // HeaderTableLayoutPanel
            // 
            this.HeaderTableLayoutPanel.BackColor = System.Drawing.Color.LightSkyBlue;
            this.HeaderTableLayoutPanel.ColumnCount = 1;
            this.HeaderTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.HeaderTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.HeaderTableLayoutPanel.Controls.Add(this.HeaderLabel, 0, 0);
            this.HeaderTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeaderTableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.HeaderTableLayoutPanel.Name = "HeaderTableLayoutPanel";
            this.HeaderTableLayoutPanel.RowCount = 1;
            this.HeaderTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.HeaderTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.HeaderTableLayoutPanel.Size = new System.Drawing.Size(964, 48);
            this.HeaderTableLayoutPanel.TabIndex = 0;
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.BackColor = System.Drawing.Color.Transparent;
            this.HeaderLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeaderLabel.ForeColor = System.Drawing.Color.White;
            this.HeaderLabel.Location = new System.Drawing.Point(3, 0);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(958, 48);
            this.HeaderLabel.TabIndex = 0;
            this.HeaderLabel.Text = "Induct Station";
            this.HeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StartPictureBox
            // 
            this.StartPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StartPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StartPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("StartPictureBox.Image")));
            this.StartPictureBox.Location = new System.Drawing.Point(4, 604);
            this.StartPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.StartPictureBox.Name = "StartPictureBox";
            this.StartPictureBox.Size = new System.Drawing.Size(97, 92);
            this.StartPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.StartPictureBox.TabIndex = 4;
            this.StartPictureBox.TabStop = false;
            this.StartPictureBox.Click += new System.EventHandler(this.StartStopButton_Click);
            // 
            // CancelPictureBox
            // 
            this.CancelPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CancelPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("CancelPictureBox.Image")));
            this.CancelPictureBox.Location = new System.Drawing.Point(4, 704);
            this.CancelPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.CancelPictureBox.Name = "CancelPictureBox";
            this.CancelPictureBox.Size = new System.Drawing.Size(97, 92);
            this.CancelPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CancelPictureBox.TabIndex = 5;
            this.CancelPictureBox.TabStop = false;
            this.CancelPictureBox.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // AutoResetAfterPanel
            // 
            this.AutoResetAfterPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlsTableLayoutPanel.SetColumnSpan(this.AutoResetAfterPanel, 2);
            this.AutoResetAfterPanel.Controls.Add(this.AutoResetAfterTableLayoutPanel);
            this.AutoResetAfterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AutoResetAfterPanel.Location = new System.Drawing.Point(3, 453);
            this.AutoResetAfterPanel.Name = "AutoResetAfterPanel";
            this.AutoResetAfterPanel.Size = new System.Drawing.Size(294, 69);
            this.AutoResetAfterPanel.TabIndex = 7;
            // 
            // AutoResetAfterTableLayoutPanel
            // 
            this.AutoResetAfterTableLayoutPanel.ColumnCount = 2;
            this.AutoResetAfterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.AutoResetAfterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.AutoResetAfterTableLayoutPanel.Controls.Add(this.AutoResetAfterNumericUpDown, 1, 1);
            this.AutoResetAfterTableLayoutPanel.Controls.Add(this.AutoResetAfterToggleSwitch, 0, 1);
            this.AutoResetAfterTableLayoutPanel.Controls.Add(this.AutoResetAfterTitleLabel, 0, 0);
            this.AutoResetAfterTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AutoResetAfterTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.AutoResetAfterTableLayoutPanel.Name = "AutoResetAfterTableLayoutPanel";
            this.AutoResetAfterTableLayoutPanel.RowCount = 2;
            this.AutoResetAfterTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.AutoResetAfterTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.AutoResetAfterTableLayoutPanel.Size = new System.Drawing.Size(292, 67);
            this.AutoResetAfterTableLayoutPanel.TabIndex = 8;
            // 
            // AutoResetAfterNumericUpDown
            // 
            this.AutoResetAfterNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.AutoResetAfterNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoResetAfterNumericUpDown.Location = new System.Drawing.Point(103, 37);
            this.AutoResetAfterNumericUpDown.Name = "AutoResetAfterNumericUpDown";
            this.AutoResetAfterNumericUpDown.Size = new System.Drawing.Size(186, 26);
            this.AutoResetAfterNumericUpDown.TabIndex = 15;
            this.AutoResetAfterNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AutoResetAfterNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // AutoResetAfterToggleSwitch
            // 
            this.AutoResetAfterToggleSwitch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.AutoResetAfterToggleSwitch.Location = new System.Drawing.Point(3, 36);
            this.AutoResetAfterToggleSwitch.Name = "AutoResetAfterToggleSwitch";
            this.AutoResetAfterToggleSwitch.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoResetAfterToggleSwitch.Properties.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AutoResetAfterToggleSwitch.Properties.Appearance.Options.UseFont = true;
            this.AutoResetAfterToggleSwitch.Properties.Appearance.Options.UseForeColor = true;
            this.AutoResetAfterToggleSwitch.Properties.OffText = "";
            this.AutoResetAfterToggleSwitch.Properties.OnText = "";
            this.AutoResetAfterToggleSwitch.Properties.ShowText = false;
            this.AutoResetAfterToggleSwitch.Size = new System.Drawing.Size(94, 29);
            this.AutoResetAfterToggleSwitch.TabIndex = 14;
            this.AutoResetAfterToggleSwitch.EditValueChanged += new System.EventHandler(this.Control_ValueChanged);
            // 
            // AutoResetAfterTitleLabel
            // 
            this.AutoResetAfterTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.AutoResetAfterTitleLabel.AutoSize = true;
            this.AutoResetAfterTableLayoutPanel.SetColumnSpan(this.AutoResetAfterTitleLabel, 2);
            this.AutoResetAfterTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoResetAfterTitleLabel.Location = new System.Drawing.Point(3, 6);
            this.AutoResetAfterTitleLabel.Name = "AutoResetAfterTitleLabel";
            this.AutoResetAfterTitleLabel.Size = new System.Drawing.Size(286, 20);
            this.AutoResetAfterTitleLabel.TabIndex = 6;
            this.AutoResetAfterTitleLabel.Text = "Auto reset after (seconds)";
            this.AutoResetAfterTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExpectedTagsPanel
            // 
            this.ExpectedTagsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlsTableLayoutPanel.SetColumnSpan(this.ExpectedTagsPanel, 2);
            this.ExpectedTagsPanel.Controls.Add(this.tableLayoutPanel1);
            this.ExpectedTagsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExpectedTagsPanel.Location = new System.Drawing.Point(3, 528);
            this.ExpectedTagsPanel.Name = "ExpectedTagsPanel";
            this.ExpectedTagsPanel.Size = new System.Drawing.Size(294, 69);
            this.ExpectedTagsPanel.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.ExpectedTagsToggleSwitch, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ExpectedTagsNumericUpDown, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ExpectedTagsLabel, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(292, 67);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ExpectedTagsLabel
            // 
            this.ExpectedTagsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ExpectedTagsLabel.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.ExpectedTagsLabel, 2);
            this.ExpectedTagsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpectedTagsLabel.Location = new System.Drawing.Point(3, 6);
            this.ExpectedTagsLabel.Name = "ExpectedTagsLabel";
            this.ExpectedTagsLabel.Size = new System.Drawing.Size(286, 20);
            this.ExpectedTagsLabel.TabIndex = 13;
            this.ExpectedTagsLabel.Text = "Expected Tags";
            this.ExpectedTagsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExpectedTagsNumericUpDown
            // 
            this.ExpectedTagsNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ExpectedTagsNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpectedTagsNumericUpDown.Location = new System.Drawing.Point(103, 37);
            this.ExpectedTagsNumericUpDown.Name = "ExpectedTagsNumericUpDown";
            this.ExpectedTagsNumericUpDown.Size = new System.Drawing.Size(186, 26);
            this.ExpectedTagsNumericUpDown.TabIndex = 14;
            this.ExpectedTagsNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ExpectedTagsNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // ExpectedTagsToggleSwitch
            // 
            this.ExpectedTagsToggleSwitch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ExpectedTagsToggleSwitch.Location = new System.Drawing.Point(3, 36);
            this.ExpectedTagsToggleSwitch.Name = "ExpectedTagsToggleSwitch";
            this.ExpectedTagsToggleSwitch.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpectedTagsToggleSwitch.Properties.Appearance.Options.UseFont = true;
            this.ExpectedTagsToggleSwitch.Properties.OffText = "";
            this.ExpectedTagsToggleSwitch.Properties.OnText = "";
            this.ExpectedTagsToggleSwitch.Properties.ShowText = false;
            this.ExpectedTagsToggleSwitch.Size = new System.Drawing.Size(94, 29);
            this.ExpectedTagsToggleSwitch.TabIndex = 15;
            this.ExpectedTagsToggleSwitch.EditValueChanged += new System.EventHandler(this.Control_ValueChanged);
            // 
            // ScanDataTableLayoutPanel
            // 
            this.ScanDataTableLayoutPanel.ColumnCount = 1;
            this.ScanDataTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ScanDataTableLayoutPanel.Controls.Add(this.TagsTextBox, 0, 2);
            this.ScanDataTableLayoutPanel.Controls.Add(this.ScanTimerValueLabel, 0, 0);
            this.ScanDataTableLayoutPanel.Controls.Add(this.ReadCountLabel, 0, 1);
            this.ScanDataTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScanDataTableLayoutPanel.Location = new System.Drawing.Point(0, 48);
            this.ScanDataTableLayoutPanel.Name = "ScanDataTableLayoutPanel";
            this.ScanDataTableLayoutPanel.RowCount = 3;
            this.ScanDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ScanDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ScanDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.ScanDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ScanDataTableLayoutPanel.Size = new System.Drawing.Size(964, 937);
            this.ScanDataTableLayoutPanel.TabIndex = 16;
            // 
            // ScanTimerValueLabel
            // 
            this.ScanTimerValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ScanTimerValueLabel.AutoSize = true;
            this.ScanTimerValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 100F);
            this.ScanTimerValueLabel.Location = new System.Drawing.Point(3, 57);
            this.ScanTimerValueLabel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 20);
            this.ScanTimerValueLabel.Name = "ScanTimerValueLabel";
            this.ScanTimerValueLabel.Size = new System.Drawing.Size(958, 153);
            this.ScanTimerValueLabel.TabIndex = 11;
            this.ScanTimerValueLabel.Text = "00:00:00.000";
            this.ScanTimerValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ReadCountLabel
            // 
            this.ReadCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ReadCountLabel.AutoSize = true;
            this.ReadCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 140F);
            this.ReadCountLabel.Location = new System.Drawing.Point(3, 296);
            this.ReadCountLabel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 20);
            this.ReadCountLabel.Name = "ReadCountLabel";
            this.ReadCountLabel.Size = new System.Drawing.Size(958, 211);
            this.ReadCountLabel.TabIndex = 12;
            this.ReadCountLabel.Text = "0";
            this.ReadCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TagsTextBox
            // 
            this.TagsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TagsTextBox.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TagsTextBox.Location = new System.Drawing.Point(3, 539);
            this.TagsTextBox.Multiline = true;
            this.TagsTextBox.Name = "TagsTextBox";
            this.TagsTextBox.ReadOnly = true;
            this.TagsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TagsTextBox.Size = new System.Drawing.Size(958, 395);
            this.TagsTextBox.TabIndex = 13;
            this.TagsTextBox.WordWrap = false;
            // 
            // OurLogoPanel
            // 
            this.OurLogoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlsTableLayoutPanel.SetColumnSpan(this.OurLogoPanel, 2);
            this.OurLogoPanel.Controls.Add(this.OurLogoPictureBox);
            this.OurLogoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OurLogoPanel.Location = new System.Drawing.Point(3, 353);
            this.OurLogoPanel.Name = "OurLogoPanel";
            this.OurLogoPanel.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.OurLogoPanel.Size = new System.Drawing.Size(294, 94);
            this.OurLogoPanel.TabIndex = 9;
            // 
            // OurLogoPictureBox
            // 
            this.OurLogoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OurLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("OurLogoPictureBox.Image")));
            this.OurLogoPictureBox.Location = new System.Drawing.Point(5, 0);
            this.OurLogoPictureBox.Name = "OurLogoPictureBox";
            this.OurLogoPictureBox.Size = new System.Drawing.Size(282, 92);
            this.OurLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.OurLogoPictureBox.TabIndex = 2;
            this.OurLogoPictureBox.TabStop = false;
            // 
            // OptimusSortersPanel
            // 
            this.ControlsTableLayoutPanel.SetColumnSpan(this.OptimusSortersPanel, 2);
            this.OptimusSortersPanel.Controls.Add(this.OptimusSortersPictureBox);
            this.OptimusSortersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptimusSortersPanel.Location = new System.Drawing.Point(3, 0);
            this.OptimusSortersPanel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.OptimusSortersPanel.Name = "OptimusSortersPanel";
            this.OptimusSortersPanel.Size = new System.Drawing.Size(294, 147);
            this.OptimusSortersPanel.TabIndex = 10;
            // 
            // OptimusSortersPictureBox
            // 
            this.OptimusSortersPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OptimusSortersPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptimusSortersPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("OptimusSortersPictureBox.Image")));
            this.OptimusSortersPictureBox.Location = new System.Drawing.Point(0, 0);
            this.OptimusSortersPictureBox.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.OptimusSortersPictureBox.Name = "OptimusSortersPictureBox";
            this.OptimusSortersPictureBox.Size = new System.Drawing.Size(294, 147);
            this.OptimusSortersPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.OptimusSortersPictureBox.TabIndex = 11;
            this.OptimusSortersPictureBox.TabStop = false;
            // 
            // InductStationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1264, 985);
            this.Controls.Add(this.ScanDataTableLayoutPanel);
            this.Controls.Add(this.HeaderPanel);
            this.Controls.Add(this.ControlsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InductStationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Induct Station";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InductStationForm_FormClosing);
            this.Load += new System.EventHandler(this.InductStationForm_Load);
            this.ControlsPanel.ResumeLayout(false);
            this.ControlsTableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustomerLogoPictureBox)).EndInit();
            this.HeaderPanel.ResumeLayout(false);
            this.HeaderTableLayoutPanel.ResumeLayout(false);
            this.HeaderTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelPictureBox)).EndInit();
            this.AutoResetAfterPanel.ResumeLayout(false);
            this.AutoResetAfterTableLayoutPanel.ResumeLayout(false);
            this.AutoResetAfterTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutoResetAfterNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoResetAfterToggleSwitch.Properties)).EndInit();
            this.ExpectedTagsPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedTagsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedTagsToggleSwitch.Properties)).EndInit();
            this.ScanDataTableLayoutPanel.ResumeLayout(false);
            this.ScanDataTableLayoutPanel.PerformLayout();
            this.OurLogoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OurLogoPictureBox)).EndInit();
            this.OptimusSortersPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptimusSortersPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer TagReadTimer;
        private System.Windows.Forms.Panel ControlsPanel;
        private System.Windows.Forms.TableLayoutPanel ControlsTableLayoutPanel;
        private System.Windows.Forms.PictureBox CustomerLogoPictureBox;
        private System.Windows.Forms.Button StartStopButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Panel HeaderPanel;
        private System.Windows.Forms.TableLayoutPanel HeaderTableLayoutPanel;
        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.PictureBox StartPictureBox;
        private System.Windows.Forms.PictureBox CancelPictureBox;
        private System.Windows.Forms.Panel AutoResetAfterPanel;
        private System.Windows.Forms.TableLayoutPanel AutoResetAfterTableLayoutPanel;
        private System.Windows.Forms.NumericUpDown AutoResetAfterNumericUpDown;
        private DevExpress.XtraEditors.ToggleSwitch AutoResetAfterToggleSwitch;
        private System.Windows.Forms.Label AutoResetAfterTitleLabel;
        private System.Windows.Forms.Panel ExpectedTagsPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.ToggleSwitch ExpectedTagsToggleSwitch;
        private System.Windows.Forms.NumericUpDown ExpectedTagsNumericUpDown;
        private System.Windows.Forms.Label ExpectedTagsLabel;
        private System.Windows.Forms.TableLayoutPanel ScanDataTableLayoutPanel;
        private System.Windows.Forms.TextBox TagsTextBox;
        private System.Windows.Forms.Label ScanTimerValueLabel;
        private System.Windows.Forms.Label ReadCountLabel;
        private System.Windows.Forms.Panel OurLogoPanel;
        private System.Windows.Forms.PictureBox OurLogoPictureBox;
        private System.Windows.Forms.Panel OptimusSortersPanel;
        private System.Windows.Forms.PictureBox OptimusSortersPictureBox;
    }
}

