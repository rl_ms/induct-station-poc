﻿using System;

using Redledge.KuehneNagel.Windows.EventArguments;

namespace Redledge.KuehneNagel.Windows.Interfaces.Managers
{
    public interface IRFIDReaderManager
    {
        bool Connect();
        bool Disconnect();
        bool IsConnected();

        event EventHandler<TagReadEventArgs> TagRead;
    }
}
