﻿using System;
using System.Windows.Forms;

using Redledge.KuehneNagel.Windows.DependencyInjection.MicrosoftExtensionsDependencyInjection;

namespace Redledge.KuehneNagel.Windows
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(DependencyInjectionContainer.Get<InductStationForm>());
        }
    }
}
