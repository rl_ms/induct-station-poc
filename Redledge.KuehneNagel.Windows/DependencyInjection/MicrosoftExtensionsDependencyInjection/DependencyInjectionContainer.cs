﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Redledge.KuehneNagel.IoC.MicrosoftExtensionsDependencyInjection;
using Redledge.KuehneNagel.Windows.Interfaces.Managers;
using Redledge.KuehneNagel.Windows.Managers;
using Redledge.KuehneNagel.Windows.POCOs;

namespace Redledge.KuehneNagel.Windows.DependencyInjection.MicrosoftExtensionsDependencyInjection
{
    public static class DependencyInjectionContainer
    {
        #region Private Properties

        static readonly IConfiguration _configuration;
        static readonly ServiceProvider _ioc;

        #endregion

        #region ctors

        static DependencyInjectionContainer()
        {
            _configuration = Container.GetConfiguration(AppContext.BaseDirectory);

            _ioc = WireUpDI(_configuration);
        }

        #endregion

        #region Public methods

        public static T Get<T>()
        {
            return _ioc.GetService<T>();
        }

        public static object GetByType(Type type)
        {
            return _ioc.GetService(type);
        }

        #endregion

        #region Private methods

        private static ServiceProvider WireUpDI(IConfiguration configuration)
        {
            // Create service collection
            var services = new ServiceCollection();

            // Wireup structure
            services.WireUpApplication(_configuration);
            services.WireUpUI(_configuration);

            // Add NLOG
            //services.AddSingleton<ILoggerFactory, LoggerFactory>();
            //services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            //services.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Trace));

            // Build DI framework
            var serviceProvider = services.BuildServiceProvider();

            // Configure NLOG
            //var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
            //loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            //NLog.LogManager.LoadConfiguration("nlog.config");

            return serviceProvider;
        }

        private static ServiceCollection WireUpUI(this ServiceCollection services, IConfiguration configuration)
        {
            var settings = new ApplicationSettingsDTO();
            configuration.GetSection("settings").Bind(settings);

            // Register UI components - Forms
            services.AddTransient<InductStationForm>();

            // Register UI components - Managers
            switch (settings.Reader?.ToLower())
            {
                case "nordic":
                    services.AddTransient<IRFIDReaderManager, NordicRFIDReaderManager>();
                    break;
                default:
                    services.AddTransient<IRFIDReaderManager, FakeRFIDReaderManager>();
                    break;
            }

            // Register UI components - Presenters

            // Register UI components - User Controls

            return services;
        }

        #endregion
    }
}
