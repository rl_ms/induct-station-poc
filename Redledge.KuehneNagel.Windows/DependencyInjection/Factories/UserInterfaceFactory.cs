﻿namespace Redledge.KuehneNagel.Windows.DependencyInjection.Factories
{
    public static class UserInterfaceFactory
    {
        public static InductStationForm CreateInductStationForm()
        {
            var rfidReader = ManagerFactory.CreateRFIDReaderManager();

            return new InductStationForm(rfidReader);
        }
    }
}
