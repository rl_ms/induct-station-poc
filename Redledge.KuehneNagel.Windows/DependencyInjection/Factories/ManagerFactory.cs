﻿using Redledge.KuehneNagel.Windows.Interfaces.Managers;
using Redledge.KuehneNagel.Windows.Managers;

namespace Redledge.KuehneNagel.Windows.DependencyInjection.Factories
{
    public static class ManagerFactory
    {
        public static IRFIDReaderManager CreateRFIDReaderManager()
        {
            return new FakeRFIDReaderManager();
        }
    }
}
